## Slotegrator test service

Web-application, based on Laravel v.8.

Веб-сервис для розыгрыша призов для авторизованных пользователей. 3 типа приза (денежный, бонусные баллы, подарки). Кол-во подарков и денежных призов ограничено.

Есть возможность переводить денежные призы в бонусные баллы. А также отправлять денежные призы на счет пользователя и бонусные баллы на счет в магазины-партнеров. Реализовано через Job. + команда в консоли для перевода денежных призов в баллы.


**Instalation**
- php artisan migrate
- php artisan db:seed --class=PresentSeeder

**Test**
- php artisan test

**Command**
- php artisan command:winSend [10]

[10] - optional limit, default = 10

Send unsended Wins throws **WinJob**

## Structure

**Controller**
- HomeController

**Models**
- User
- Present
- Win

**Job**
- /app/Jobs/**WinJob**

**Service, using for send**
- /app/Services/**WinService**
