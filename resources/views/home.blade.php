@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    {{ $user->name }}, {{ __('You are logged in!') }}
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div>
                        @if ($user->win()->exists())
                            <div>
                                <div class="mb-3">
                                    Your prize:
                                    @if ($user->win->type == 1)

                                        $ {{ $user->win->money }}

                                        @if (!$user->win->refused_at && !$user->win->send_at)
                                            <form action="{{ route('send') }}" method="post">
                                                @csrf
                                                <button class="btn btn-sm btn-outline-success" type="submit">Send to the bank</button>
                                            </form>

                                            <form action="{{ route('convert') }}" method="post">
                                                @csrf
                                                <button class="btn btn-sm btn-outline-info" type="submit">Convert to points</button>
                                            </form>
                                        @endif

                                    @elseif ($user->win->type == 2)

                                        {{ $user->win->points }} points
                                        @if ($user->win->convert_at)
                                            (was converted)
                                        @endif


                                        @if (!$user->win->refused_at && !$user->win->send_at)
                                            <form action="{{ route('send') }}" method="post">
                                                @csrf
                                                <button class="btn btn-sm btn-outline-success" type="submit">Send to the application</button>
                                            </form>
                                        @endif

                                    @elseif ($user->win->type == 3)
                                        {{ $user->win->present->name }}
                                    @endif
                                </div>

                                @if ($user->win->send_at)
                                    <div class="alert alert-warning">
                                        {{ __('You already send') }}
                                    </div>
                                @elseif ($user->win->refused_at)
                                    <div class="alert alert-warning">
                                        {{ __('You already refused') }}
                                    </div>
                                @else
                                    <form action="{{ route('refuse') }}" method="post">
                                        @csrf
                                        <div class="text-center">
                                            <button class="btn btn-outline-danger" type="submit">Refuse Prize</button>
                                        </div>
                                    </form>
                                @endif
                            </div>
                        @else
                            <form action="{{ route('prize') }}" method="post">
                            @csrf
                                <div class="text-center">
                                    <button class="btn btn-outline-success" type="submit">Get Prize!</button>
                                </div>
                            </form>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
