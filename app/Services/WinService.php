<?php
namespace App\Services;

use App\Models\Win;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;

Class WinService
{
    static public function send(int $winId): bool
    {
        $win = Win::findOrFail($winId);

        if ($win->type == Win::TYPE_PRESENT) {

            $win->send_at = Carbon::now();
            $win->save();
            return true;

        } else {
            // Send request
            $response = Http::get(config('slotegrator.url.' . $win->type), [
                'user_id' => $win->user->id,
                'account' => $win->user->bank_account,
            ]);

            // Response hove 200
            if ($response->successful()) {
                $win->send_at = Carbon::now();
                $win->save();
                return true;
            }

        }

        return false;
    }
}
