<?php

namespace App\Console\Commands;

use App\Jobs\WinJob;
use App\Models\Win;
use Illuminate\Console\Command;

class WinCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:winSend {limit=10}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send users wins to the bank';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment('Start');

        // Select unsended wins
        $wins = Win::select('id', 'user_id', 'type', 'money', 'points')
            ->with([
                'user' => function ($q) {
                    $q->select(['id', 'bank_account']);
                }
            ])
            ->has('user')
            ->whereIn('type', [Win::TYPE_MONEY, Win::TYPE_POINT])
            ->whereNull('send_at')
            ->limit($this->argument('limit'))
            ->get();

        foreach($wins as $win) {

            $this->line($win);
            // Send to JOB
            dispatch((new WinJob($win->id)));
        }

        $this->comment('Stop');
        return true;
    }
}
