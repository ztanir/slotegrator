<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\DocBlock\Tags\Throws;

/**
 * Class Win
 * @package App\Models
 * @property int $id
 * @property int $type
 * @property int $user_id
 * @property int|null $present_id
 * @property int|null $money
 * @property int|null $points
 * @property string|null $refused_at
 * @property string|null $convert_at
 * @property string|null $send_at
 */
class Win extends Model
{
    use HasFactory;

    /**
     *  Types of prize
     */
    public const TYPE_MONEY = 1;
    public const TYPE_POINT = 2;
    public const TYPE_PRESENT = 3;

    protected $table = 'wins';

    protected $fillable = [
        'type',
        'user_id',
        'present_id',
        'money',
        'points',

        'refused_at',
        'convert_at',
        'send_at',
    ];

    // Get total money
    static public function totalMoney()
    {
        return Win::where('type', Win::TYPE_MONEY)->sum('money');
    }

    // Get total presents
    static public function totalPresents()
    {
        return Win::where('type', Win::TYPE_PRESENT)->count('present_id');
    }

    // Prepare available types
    static public function availableTypes(): array
    {
        $types = [Win::TYPE_POINT];

        // Check available money (+ min, which will be accrued)
        if ((Win::totalMoney() + config('slotegrator.money.min')) < config('slotegrator.limit.money')) {
            array_push($types, Win::TYPE_MONEY);
        }

        // Check available presents
        if (Win::totalPresents() < config('slotegrator.limit.presents')) {
            array_push($types, Win::TYPE_PRESENT);
        }

        return $types;
    }

    /**
     * @param User $user
     * @param int|null $type
     * @return Win|null
     */
    static public function generate(User $user, int $type = null): ?Win
    {

        // Already have prize
        if ($user->win()->exists()) {
            return null;
        }

        // New Win object
        $win = new Win();
        $win->user_id = $user->id;

        if ($type) {
            $win->type = $type;
        } else {
            // Generate available type of prize
            $types = self::availableTypes();
            $win->type = $types[array_rand($types, 1)];
        }

        switch($win->type) {

            case Win::TYPE_MONEY:

                // Check MAX, must be less than the limit
                $max = config('slotegrator.money.max');
                if ((Win::totalMoney() + $max) > config('slotegrator.limit.money')) {
                    $max = config('slotegrator.limit.money') - Win::totalMoney();
                }
                $win->money = rand(config('slotegrator.money.min'), $max);
                break;

            case Win::TYPE_POINT:
                $win->points = rand(config('slotegrator.point.min'), config('slotegrator.point.max'));
                break;

            case Win::TYPE_PRESENT:
                $present = Present::select('id')->inRandomOrder()->first();
                $win->present_id = $present->id;
                break;
        }

        $win->save();

        return $win;
    }

    /**
     * @param int $winId
     * @return bool
     */
    static public function convert(int $winId): bool
    {
        Win::find($winId)
            ->where('type', Win::TYPE_MONEY)
            ->update([
                'points' => DB::raw('(money * ' . config('slotegrator.ratio') . ')'),
                'type' => Win::TYPE_POINT,
                'convert_at' => Carbon::now()
            ]);
        return true;
    }

    /**
     * @param int $userId
     * @return bool
     */
    static public function refuse(int $userId): bool
    {
        Win::where('user_id', $userId)
            ->update(['refused_at' => Carbon::now()]);
        return true;
    }

    /**
     * @return HasOne
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function present(): BelongsTo
    {
        return $this->belongsTo(Present::class);
    }
}
