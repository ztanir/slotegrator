<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Present
 * @package App\Models
 * @property int $id
 * @property string $name
 * @property int $count
 */
class Present extends Model
{
    use HasFactory;

    protected $table = 'presents';

    protected $fillable = [
        'name',
        'count',
    ];
}
