<?php

namespace App\Jobs;

use App\Services\WinService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class WinJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $win_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $winId)
    {
        $this->win_id = $winId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        WinService::send($this->win_id);
    }
}
