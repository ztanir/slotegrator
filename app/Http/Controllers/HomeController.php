<?php

namespace App\Http\Controllers;

use App\Jobs\WinJob;
use App\Models\Win;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home', ['user' => Auth::user()]);
    }

    // Generate the prize
    public function prize()
    {
        Win::generate(Auth::user());
        return redirect('home');
    }

    // Refuse the prize
    public function refuse()
    {
        Win::refuse(Auth::id());
        return redirect('home');
    }

    // Convert money to points
    public function convert()
    {
        Win::convert(Auth::user()->win->id);
        return redirect('home');
    }

    // Send to the bank or application
    public function send()
    {
        dispatch((new WinJob(Auth::user()->win->id)));
        return redirect('home');
    }
}
