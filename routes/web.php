<?php

use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return redirect('/home');
});

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::post('/prize', [HomeController::class, 'prize'])->name('prize');
Route::post('/refuse', [HomeController::class, 'refuse'])->name('refuse');
Route::post('/convert', [HomeController::class, 'convert'])->name('convert');
Route::post('/send', [HomeController::class, 'send'])->name('send');
