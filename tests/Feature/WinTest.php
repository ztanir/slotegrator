<?php

namespace Tests\Feature;

use App\Jobs\WinJob;
use App\Models\User;
use App\Models\Win;
use App\Services\WinService;
use Database\Factories\UserFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class WinTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_access()
    {
        $response = $this->get('/');
        $response->assertStatus(302);
    }

    /**
     * Create user
     *
     * @return void
     */
    public function test_create_user()
    {
        $user = User::factory()->create();
        $this->assertEquals(1, $user->id);
    }

    /**
     * Create win
     *
     * @return void
     */
    public function test_create_win()
    {
        $user = User::factory()->create();
        $win = Win::generate($user, Win::TYPE_MONEY);
        $this->assertEquals(1, $win->id);
    }

    /**
     * Refuse win
     *
     * @return void
     */
    public function test_refuse_win()
    {
        $user = User::factory()->create();
        Win::generate($user, Win::TYPE_MONEY);
        $result = Win::refuse($user->id);
        $this->assertEquals(true, $result);
    }

    /**
     * Convert win
     *
     * @return void
     */
    public function test_convert_win()
    {
        $user = User::factory()->create();
        $win = Win::generate($user, Win::TYPE_MONEY);
        $result = Win::convert($win->id);
        $this->assertEquals(true, $result);
    }

    /**
     * Send win
     *
     * @return void
     */
    public function test_send_win()
    {
        $user = User::factory()->create();
        $win = Win::generate($user, Win::TYPE_MONEY);
        $result = WinService::send($win->id);
        $this->assertEquals(true, $result);
    }


    /**
     * Convert win
     *
     * @return void
     */
    public function test_convert_win_with_values()
    {
        $user = User::factory()->create();
        $win = Win::generate($user, Win::TYPE_MONEY);

        $waiting = ($win->money * config('slotegrator.ratio'));
        Win::convert($win->id);

        $win = Win::find($win->id);
        $this->assertEquals($waiting, $win->points);
    }
}
