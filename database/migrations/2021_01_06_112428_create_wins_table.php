<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wins', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type');
            $table->integer('user_id');
            $table->integer('present_id')->nullable();
            $table->integer('money')->nullable();
            $table->integer('points')->nullable();
            $table->timestamps();

            $table->dateTime('refused_at')->nullable();
            $table->dateTime('convert_at')->nullable();
            $table->dateTime('send_at')->nullable();

            $table->index('type');
            $table->index('user_id');
            $table->index('present_id');

            //Не стал делать внешние ключи на данном этапе
            // $table->foreign('user_id')->references('id')->on('users');
            // $table->foreign('present_id')->references('id')->on('presents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wins');
    }
}
